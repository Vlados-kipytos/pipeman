package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"syscall"
)

type Settings struct {
	ServerMode string `json:"serverMode"`
	SourceDir  string `json:"sourceDir"`
	TargetDir  string `json:"targetDir"`
}

var pipeFile = "tools/pipe2.log"

func scheduleRead() {
	for {

		rawDataIn, err := ioutil.ReadFile(pipeFile)
		if err != nil {
			log.Fatal("Cannot load settings:", err)
		}

		var settings Settings
		err = json.Unmarshal(rawDataIn, &settings)
		if err != nil {
			continue
		}

		rawDataOut, err := json.MarshalIndent(&settings, "", "  ")
		if err != nil {
			log.Fatal("JSON marshaling failed:", err)
		}


		err = ioutil.WriteFile("docs/app3.log", rawDataOut, 0)
		if err != nil {
			log.Fatal("Cannot write updated settings file:", err)
		}

	}


}


func main() {

	os.Remove(pipeFile)
	err := syscall.Mkfifo(pipeFile, 0666)
	if err != nil {
		log.Fatal("Make named pipe file error:", err)
	}
	go scheduleRead()
	fmt.Println("open a named pipe file for read.")
	file, err := os.OpenFile(pipeFile, os.O_RDONLY, os.ModeNamedPipe)
	if err != nil {
		log.Fatal("Open named pipe file error:", err)
	}

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadBytes('\n')
		if err == nil {
			fmt.Print("load string:" + string(line))
		}
	}

}

