package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"syscall"
	"time"
)

type settings struct {
	ServerMode string `json:"serverMode"`
	SourceDir  string `json:"sourceDir"`
	TargetDir  string `json:"targetDir"`
}

var pipeFile = "tools/pipe.log"

func main() {
	os.Remove(pipeFile)
	err := syscall.Mkfifo(pipeFile, 0666)
	if err != nil {
		log.Fatal("Make named pipe file error:", err)
	}
	go scheduleWrite()
	fmt.Println("open a named pipe file for read.")
	file, err := os.OpenFile(pipeFile, os.O_RDONLY, os.ModeNamedPipe)
	if err != nil {
		log.Fatal("Open named pipe file error:", err)
	}

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadBytes('\n')
		if err == nil {
			fmt.Print("load string:" + string(line))
		}
	}
}

func scheduleWrite() {
	data := settings{
		ServerMode:    "Cool",
		SourceDir:   "Standart",
		TargetDir:      "Man",
	}
	fmt.Println("start schedule writing.")
	i := 0
	for {
		fmt.Println("write string to named pipe file.")
		file, _ := json.MarshalIndent(data, "", " ")
		_ = ioutil.WriteFile("tools/pipe.log", file, 0644)
		fi, _ := json.MarshalIndent(data, "", " ")
		_ = ioutil.WriteFile("docs/app1.log", fi, 0644)
		time.Sleep(time.Second)
		i++
	}
}