package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"syscall"
	"time"
)

type Settings struct {
	ServerMode string `json:"serverMode"`
	SourceDir  string `json:"sourceDir"`
	TargetDir  string `json:"targetDir"`
}

var pipeFile = "tools/pipe2.log"


func scheduleWrite() {
	fmt.Println("start schedule writing.")
	f, err := os.OpenFile(pipeFile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0777)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	i := 0
	for {
		f.WriteString("")
		time.Sleep(time.Second)
		i++
	}
}

func scheduleRead() {

	data := Settings{
		ServerMode:    "Cool",
		SourceDir:   "Standart",
		TargetDir:      "Man",
	}
	b, _ := json.Marshal(data)

	var value = string(b)
	new_data := Settings{}
	json.Unmarshal([]byte(value), &new_data)

	var data_new = Settings{}

    for {

		rawDataIn, err := ioutil.ReadFile("tools/pipe.log")
		if err != nil {
			log.Fatal("Cannot load settings:", err)
		}

		var settings Settings
		err = json.Unmarshal(rawDataIn, &settings)
		if err != nil {
			continue
		}

		rawDataOut, err := json.MarshalIndent(&settings, "", "  ")
		if err != nil {
			log.Fatal("JSON marshaling failed:", err)
		}

		var new_value = string(rawDataOut)
		json.Unmarshal([]byte(new_value), &data_new)

		if data_new.TargetDir != new_data.TargetDir {

			err = ioutil.WriteFile("docs/app2.log", rawDataOut, 0)
			if err != nil {
				log.Fatal("Cannot write updated settings file:", err)
			}


		} else {

			err = ioutil.WriteFile("tools/pipe2.log", rawDataOut, 0)
			if err != nil {
				log.Fatal("Cannot write updated settings file:", err)
			}

		}


	}


	}


func main() {

	os.Remove(pipeFile)
	err := syscall.Mkfifo(pipeFile, 0666)
	if err != nil {
		log.Fatal("Make named pipe file error:", err)
	}
	go scheduleWrite()
	go scheduleRead()
	fmt.Println("open a named pipe file for read.")
	file, err := os.OpenFile(pipeFile, os.O_RDONLY, os.ModeNamedPipe)
	if err != nil {
		log.Fatal("Open named pipe file error:", err)
	}

	reader := bufio.NewReader(file)

	for {
		line, err := reader.ReadBytes('\n')
		if err == nil {
			fmt.Print("load string:" + string(line))
		}
	}

}

